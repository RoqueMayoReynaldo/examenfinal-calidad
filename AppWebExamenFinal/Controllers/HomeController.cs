﻿using AppWebExamenFinal.Models;
using AppWebExamenFinal.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AppWebExamenFinal.Controllers
{

    public class GastosTotales
    {
        public float Egresos { get; set; }
        public float Ingresos { get; set; }

    }
    public class HomeController : Controller
    {
        private readonly ICuentaRepository cuentaRepository;
        private readonly IIngresoRepository ingresoRepository;
        private readonly IGastoRepository gastoRepository;

        public HomeController(ICuentaRepository cuentaRepository,IIngresoRepository ingresoRepository,IGastoRepository gastoRepository)
        {
            this.cuentaRepository = cuentaRepository;
            this.ingresoRepository = ingresoRepository;
            this.gastoRepository = gastoRepository;
        }
        public IActionResult Index()
        {
            GastosTotales gt = new GastosTotales
            {
                Egresos = gastoRepository.montoTotal(),
                Ingresos = ingresoRepository.montoTotal()
            };



            return View("Index",gt);
        }
        [HttpGet]
        public IActionResult CrearCuenta()
        {



            return View("CrearCuenta");
        }
        [HttpPost]
        public IActionResult CrearCuenta(Cuenta cuenta)
        {

            cuentaRepository.addCuenta(cuenta);



            GastosTotales gt = new GastosTotales
            {
                Egresos = gastoRepository.montoTotal(),
                Ingresos = ingresoRepository.montoTotal()
            };
            return View("Index",gt);
        }

        [HttpGet]
        public IActionResult CrearGasto()
        {



            return View("CrearGasto");
        }
        [HttpPost]
        public IActionResult CrearGasto(Gasto gasto)
        {

            Cuenta cuenta = cuentaRepository.findByName(gasto.nombreCuenta);

            if (cuenta==null )
            {
                ModelState.AddModelError("Msj","No existe la cuenta");
               
            }

            if (gasto.monto> cuenta.saldo)
            {
                ModelState.AddModelError("Msj", "El monto excede los fondos de la cuenta");

            }

            if (ModelState.IsValid)
            {
                cuenta.saldo -= gasto.monto;

                cuentaRepository.updateCuenta(cuenta);


                gastoRepository.addGasto(gasto);

                GastosTotales gt = new GastosTotales
                {
                    Egresos = gastoRepository.montoTotal(),
                    Ingresos = ingresoRepository.montoTotal()
                };
                return View("Index",gt);
            }

            return View("CrearGasto");

        }

        [HttpGet]
        public IActionResult ListarGastos()
        {



            return View("ListarGastos",gastoRepository.getAll());
        }

        [HttpGet]
        public IActionResult CrearIngreso()
        {



            return View("CrearIngreso");
        }
        [HttpPost]
        public IActionResult CrearIngreso(Ingreso ingreso)
        {

            Cuenta cuenta = cuentaRepository.findByName(ingreso.nombreCuenta);

            if (cuenta == null)
            {
                ModelState.AddModelError("Msj", "No existe la cuenta");

            }

            if (ModelState.IsValid)
            {
                cuenta.saldo += ingreso.monto;

                cuentaRepository.updateCuenta(cuenta);


                ingresoRepository.addIngreso(ingreso);

                GastosTotales gt = new GastosTotales
                {
                    Egresos = gastoRepository.montoTotal(),
                    Ingresos = ingresoRepository.montoTotal()
                };
                return View("Index",gt);
            }

            return View("CrearIngreso");

        }

        [HttpGet]
        public IActionResult ListarIngresos()
        {



            return View("ListarIngresos", ingresoRepository.getAll());
        }

    }
}
