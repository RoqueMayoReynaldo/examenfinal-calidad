﻿using AppWebExamenFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWebExamenFinal.Repository
{

    public interface ICuentaRepository
    {

       
        public void addCuenta(Cuenta cuenta);
        public Cuenta findByName(string name);
        public void updateCuenta(Cuenta cuenta); 
      
    }

    public class CuentaRepository : ICuentaRepository
    {

        public static List<Cuenta> cuentas = new List<Cuenta>
        {

            new Cuenta { id=1,nombre="Ahorros",categoria="Propio",saldo=604}


        };

        public void addCuenta(Cuenta cuenta)
        {

            cuentas.Add(cuenta);
        }

        public Cuenta findByName(string name)
        {
            return cuentas.FirstOrDefault(o=>o.nombre==name);
        }

        public void updateCuenta(Cuenta cuenta)
        {
            cuentas.FirstOrDefault(o=>o.nombre==cuenta.nombre).saldo=cuenta.saldo;
        }
       
    }
}
