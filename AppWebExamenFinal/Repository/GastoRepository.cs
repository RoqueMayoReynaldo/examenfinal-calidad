﻿using AppWebExamenFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWebExamenFinal.Repository
{
    public interface IGastoRepository {


        public List<Gasto> getAll();
        public void addGasto(Gasto gasto);

        public float montoTotal();
    }

    public class GastoRepository : IGastoRepository
    {
        public static List<Gasto> gastos = new List<Gasto> { 
       
        

        
        
        };

        public void addGasto(Gasto gasto)
        {
            gasto.fechaHora = DateTime.Now;

            gastos.Add(gasto);
        }

        public List<Gasto> getAll()
        {
            return gastos.ToList();
        }

        public float montoTotal()
        {
            return gastos.Select(o=>o.monto).Sum();
        }
    }
}
