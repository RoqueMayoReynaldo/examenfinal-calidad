﻿using AppWebExamenFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWebExamenFinal.Repository
{
    public interface IIngresoRepository
    {
        public List<Ingreso> getAll();
        public void addIngreso(Ingreso ingreso);

        public float montoTotal();
    }

    public class IngresoRepository : IIngresoRepository
    {

        public static List<Ingreso> ingresos = new List<Ingreso>
        {

        };

        public void addIngreso(Ingreso ingreso)
        {
            ingresos.Add(ingreso);
        }

        public List<Ingreso> getAll()
        {
            return ingresos.ToList();
        }

        public float montoTotal()
        {
            return ingresos.Select(o => o.monto).Sum();
        }
    }
}
