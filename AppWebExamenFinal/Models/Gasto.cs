﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWebExamenFinal.Models
{
    public class Gasto
    {
        public int id { get; set; }
        public DateTime fechaHora { get; set; }
        public string descripcion { get; set; }
        public float monto { get; set; }

        public string nombreCuenta { get; set; }
       

    }

}
