﻿using AppWebExamenFinal.Controllers;
using AppWebExamenFinal.Models;
using AppWebExamenFinal.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppWebExamenFinal.Test.ControllersTest
{
    public class HomeControllerTest
    {
        [Test]
        public void CrearCuentaGetCase01()
        {



            HomeController hc = new HomeController(null,null,null);
            var result = hc.CrearCuenta();

            Assert.IsNotNull(result);
            

        }

        [Test]
        public void CrearCuentaGetCase02()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearCuenta();

            Assert.IsInstanceOf<ViewResult>(result);


        }

        [Test]
        public void CrearCuentaGetCase03()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearCuenta() as ViewResult;

            Assert.AreEqual("CrearCuenta",result.ViewName);


        }
        [Test]
        public void CrearCuentaGetCase04()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearCuenta() as ViewResult;

            Assert.IsNull( result.Model);


        }


        [Test]
        public void CrearCuentaPostCase01()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();
         

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta());



            Assert.IsNotNull(result);


        }

        [Test]
        public void CrearCuentaPostCase02()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta());



            Assert.IsInstanceOf<ViewResult>(result);


        }
        [Test]
        public void CrearCuentaPostCase03()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;



            Assert.AreEqual("Index",result.ViewName);


        }
        [Test]
        public void CrearCuentaPostCase04()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;



            Assert.IsNotNull(result.Model);


        }

        public void CrearCuentaPostCase05()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;



            Assert.IsInstanceOf<GastosTotales>(result.Model);


        }

        [Test]
        public void CrearCuentaPostCase06()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            mockGastoRepo.Setup(o=>o.montoTotal()).Returns(500);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;

            var modelo = result.Model as GastosTotales;




            Assert.AreEqual(500,modelo.Egresos);
            Assert.AreEqual(0, modelo.Ingresos);

        }

        [Test]
        public void CrearCuentaPostCase07()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            mockGastoRepo.Setup(o => o.montoTotal()).Returns(0);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(600);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;

            var modelo = result.Model as GastosTotales;




            Assert.AreEqual(0, modelo.Egresos);
            Assert.AreEqual(600, modelo.Ingresos);

        }

        [Test]
        public void CrearCuentaPostCase08()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            mockGastoRepo.Setup(o => o.montoTotal()).Returns(0);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;

            var modelo = result.Model as GastosTotales;




            Assert.AreEqual(0, modelo.Egresos);
            Assert.AreEqual(0, modelo.Ingresos);

        }
        [Test]
        public void CrearCuentaPostCase09()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            mockGastoRepo.Setup(o => o.montoTotal()).Returns(1000);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(1000);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;

            var modelo = result.Model as GastosTotales;




            Assert.AreEqual(1000, modelo.Egresos);
            Assert.AreEqual(1000, modelo.Ingresos);

        }
        [Test]
        public void CrearCuentaPostCase10()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();


            mockGastoRepo.Setup(o => o.montoTotal()).Returns(0.50f);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0.50f);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearCuenta(new Cuenta()) as ViewResult;

            var modelo = result.Model as GastosTotales;




            Assert.AreEqual(0.50, modelo.Egresos);
            Assert.AreEqual(0.50, modelo.Ingresos);

        }


        //gastos listar y registrar


        [Test]
        public void CrearGastoGetCase01()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearGasto();

            Assert.IsNotNull(result);


        }

        [Test]
        public void CrearGastoGetCase02()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearGasto();

            Assert.IsInstanceOf<ViewResult>(result);


        }

        [Test]
        public void CrearGastoGetCase03()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearGasto() as ViewResult;

            Assert.AreEqual("CrearGasto", result.ViewName);


        }
        [Test]
        public void CrearGastoGetCase04()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearGasto() as ViewResult;

            Assert.IsNull(result.Model);


        }



        

        //probamos que no se puede exceder los fondos de la cuenta


        [Test]
        public void CrearGastoPostCaseFailExcede01()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();


            Cuenta cuenta = new Cuenta { saldo=1000};
            mockCuentaRepo.Setup(O=>O.findByName(It.IsAny<string>())).Returns( cuenta);
            


            HomeController hc = new HomeController(mockCuentaRepo.Object, null, null);
            var result = hc.CrearGasto(new Gasto { monto=5000}) as ViewResult;

            Assert.IsFalse(result.ViewData.ModelState.IsValid);


        }

        [Test]
        public void CrearGastoPostCaseFailExcede02()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();


            Cuenta cuenta = new Cuenta { saldo = 50 };
            mockCuentaRepo.Setup(O => O.findByName(It.IsAny<string>())).Returns(cuenta);



            HomeController hc = new HomeController(mockCuentaRepo.Object, null, null);
            var result = hc.CrearGasto(new Gasto { monto = 50.01f }) as ViewResult;

            Assert.IsFalse(result.ViewData.ModelState.IsValid);


        }
        [Test]
        public void CrearGastoPostCaseFailExcede03()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();


            Cuenta cuenta = new Cuenta { saldo = 0 };
            mockCuentaRepo.Setup(O => O.findByName(It.IsAny<string>())).Returns(cuenta);



            HomeController hc = new HomeController(mockCuentaRepo.Object, null, null);
            var result = hc.CrearGasto(new Gasto { monto = 100 }) as ViewResult;

            Assert.IsFalse(result.ViewData.ModelState.IsValid);


        }

        [Test]
        public void CrearGastoPostCaseFailExcede04()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();


            Cuenta cuenta = new Cuenta { saldo = 0 };
            mockCuentaRepo.Setup(O => O.findByName(It.IsAny<string>())).Returns(cuenta);



            HomeController hc = new HomeController(mockCuentaRepo.Object, null, null);
            var result = hc.CrearGasto(new Gasto { monto = 100 }) as ViewResult;

            Assert.AreEqual("El monto excede los fondos de la cuenta", result.ViewData.ModelState["Msj"].Errors[0].ErrorMessage);


        }


        [Test]
        public void CrearGastoPostCaseSuccess01()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o=>o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(50);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto()) as ViewResult;


            Assert.IsNotNull(result.Model);


        }

        [Test]
        public void CrearGastoPostCaseSuccess02()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(50);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto());


            Assert.IsInstanceOf<ViewResult>(result);


        }
        [Test]
        public void CrearGastoPostCaseSuccess03()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(50);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto()) as ViewResult;


            Assert.AreEqual("Index",result.ViewName);


        }

        [Test]
        public void CrearGastoPostCaseSuccess04()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(50);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto()) as ViewResult;


            Assert.IsTrue(result.ViewData.ModelState.IsValid);


        }

        [Test]
        public void CrearGastoPostCaseSuccess05()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();

            Cuenta cuenta = new Cuenta {saldo=500 };

            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(cuenta);
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(100);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto { monto=50}) as ViewResult;


            Assert.AreEqual(450,cuenta.saldo);


        }

        [Test]
        public void CrearGastoPostCaseSuccess06()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();

            Cuenta cuenta = new Cuenta { saldo = 430.50f};

            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(cuenta);
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(100);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto { monto = 0.50f }) as ViewResult;


            Assert.AreEqual(430, cuenta.saldo);


        }

        [Test]
        public void CrearGastoPostCaseSuccess07()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();

            Cuenta cuenta = new Cuenta { saldo = 100 };

            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(cuenta);
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(100);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearGasto(new Gasto { monto = 100 }) as ViewResult;


            Assert.AreEqual(0, cuenta.saldo);


        }



        [Test]
        public void ListarGastosCase01()
        {

           
            var mockGastoRepo = new Mock<IGastoRepository>();

           

        
            

            HomeController hc = new HomeController(null, null, mockGastoRepo.Object);
            var result = hc.ListarGastos();


            Assert.IsNotNull(result);


        }

        [Test]
        public void ListarGastosCase02()
        {


            var mockGastoRepo = new Mock<IGastoRepository>();




            mockGastoRepo.Setup(o => o.getAll()).Returns(new List<Gasto>());


            HomeController hc = new HomeController(null, null, mockGastoRepo.Object);
            var result = hc.ListarGastos();


            Assert.IsInstanceOf<ViewResult>(result);


        }


        [Test]
        public void ListarGastosCase03()
        {


            var mockGastoRepo = new Mock<IGastoRepository>();

            mockGastoRepo.Setup(o => o.getAll()).Returns(new List<Gasto>()) ;


           


            HomeController hc = new HomeController(null, null, mockGastoRepo.Object);
            var result = hc.ListarGastos() as ViewResult;


            Assert.IsNotNull(result.Model);


        }
        [Test]
        public void ListarGastosCase04()
        {


            var mockGastoRepo = new Mock<IGastoRepository>();


            mockGastoRepo.Setup(o => o.getAll()).Returns(new List<Gasto>());
       


            HomeController hc = new HomeController(null, null, mockGastoRepo.Object);
            var result = hc.ListarGastos() as ViewResult;
            mockGastoRepo.Setup(o => o.getAll()).Returns(new List<Gasto>());

            Assert.IsInstanceOf<List<Gasto>>(result.Model);


        }






        //crear ingreso y listar







        [Test]
        public void CrearIngresoGetCase01()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearIngreso();

            Assert.IsNotNull(result);


        }

        [Test]
        public void CrearIngresoGetCase02()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearIngreso();

            Assert.IsInstanceOf<ViewResult>(result);


        }

        [Test]
        public void CrearIngresoGetCase03()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearIngreso() as ViewResult;

            Assert.AreEqual("CrearIngreso", result.ViewName);


        }
        [Test]
        public void CrearIngresoGetCase04()
        {



            HomeController hc = new HomeController(null, null, null);
            var result = hc.CrearIngreso() as ViewResult;

            Assert.IsNull(result.Model);


        }



        [Test]
        public void CrearIngresoPostFailCase01()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();



            HomeController hc = new HomeController(mockCuentaRepo.Object, null, null);
            var result = hc.CrearIngreso(new Ingreso { monto = 1000 }) as ViewResult;

            Assert.IsFalse(result.ViewData.ModelState.IsValid);


        }



        [Test]
        public void CrearIngresoPostFailCase02()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();



            HomeController hc = new HomeController(mockCuentaRepo.Object, null, null);
            var result = hc.CrearIngreso(new Ingreso { monto = 1000 }) as ViewResult;

            Assert.AreEqual("CrearIngreso",result.ViewName);


        }





        [Test]
        public void CrearIngresoPostCaseSuccess01()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(60);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso()) as ViewResult;


            Assert.IsNotNull(result.Model);


        }

        [Test]
        public void CrearIngresoPostCaseSuccess02()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(30);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso()) as ViewResult;


            Assert.IsInstanceOf<ViewResult>(result);


        }
        [Test]
        public void CrearIngresoPostCaseSuccess03()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(50);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso()) as ViewResult;


            Assert.AreEqual("Index", result.ViewName);


        }

        [Test]
        public void CrearIngresoPostCaseSuccess04()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();



            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(new Cuenta());
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(780);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso()) as ViewResult;


            Assert.IsTrue(result.ViewData.ModelState.IsValid);


        }

        [Test]
        public void CrearIngresoPostCaseSuccess05()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();

            Cuenta cuenta = new Cuenta { saldo = 500 };

            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(cuenta);
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(100);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso { monto = 50 }) as ViewResult;


            Assert.AreEqual(550, cuenta.saldo);


        }

        [Test]
        public void CrearIngresoPostCaseSuccess06()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();

            Cuenta cuenta = new Cuenta { saldo = 430.50f };

            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(cuenta);
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(100);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso { monto = 0.50f }) as ViewResult;


            Assert.AreEqual(431, cuenta.saldo);


        }

        [Test]
        public void CrearIngresoPostCaseSuccess07()
        {

            var mockCuentaRepo = new Mock<ICuentaRepository>();
            var mockIngresoRepo = new Mock<IIngresoRepository>();
            var mockGastoRepo = new Mock<IGastoRepository>();

            Cuenta cuenta = new Cuenta { saldo = 100 };

            mockCuentaRepo.Setup(o => o.findByName(It.IsAny<string>())).Returns(cuenta);
            mockGastoRepo.Setup(o => o.montoTotal()).Returns(100);
            mockIngresoRepo.Setup(o => o.montoTotal()).Returns(0);

            HomeController hc = new HomeController(mockCuentaRepo.Object, mockIngresoRepo.Object, mockGastoRepo.Object);
            var result = hc.CrearIngreso(new Ingreso { monto = 100 }) as ViewResult;


            Assert.AreEqual(200, cuenta.saldo);


        }



        [Test]
        public void ListarIngresosCase01()
        {


            var mockIngresoRepo = new Mock<IIngresoRepository>();



          


            HomeController hc = new HomeController(null, mockIngresoRepo.Object,null);
            var result = hc.ListarIngresos();


            Assert.IsNotNull(result);


        }

        [Test]
        public void ListarIngresosCase02()
        {


            var mockIngresoRepo = new Mock<IIngresoRepository>();



            


            HomeController hc = new HomeController(null, mockIngresoRepo.Object, null);
            var result = hc.ListarIngresos();

            Assert.IsInstanceOf<ViewResult>(result);


        }


        [Test]
        public void ListarIngresosCase03()
        {

            var mockIngresoRepo = new Mock<IIngresoRepository>();



            mockIngresoRepo.Setup(o => o.getAll()).Returns(new List<Ingreso>());


            HomeController hc = new HomeController(null, mockIngresoRepo.Object, null);
            var result = hc.ListarIngresos() as ViewResult;


            Assert.IsNotNull(result.Model);


        }
        [Test]
        public void ListarIngresosCase04()
        {


            var mockIngresoRepo = new Mock<IIngresoRepository>();



            mockIngresoRepo.Setup(o => o.getAll()).Returns(new List<Ingreso>());


            HomeController hc = new HomeController(null, mockIngresoRepo.Object, null);
            var result = hc.ListarIngresos() as ViewResult;

           

    

            Assert.IsInstanceOf<List<Ingreso>>(result.Model);


        }


    }
    }
